(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.brain :as brain])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit" [msg]
  (print (str "\n\n" (prn-str msg)))
  (reset! brain/game-data msg)
  (reset! brain/current-location {:piece 0 :location 0.0 :speed 0.0 :angle 0.0})
  (swap! brain/track-parameters
         assoc
         :piece-count
         (count (get-in msg [:data :race :track :pieces])))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "yourCar" [msg]
  (swap! brain/track-parameters assoc :color (get-in msg [:data :color]))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "carPositions" [msg]
  (brain/handle-msg msg))

(defmethod handle-msg "turboAvailable" [msg]
  (swap! brain/track-parameters assoc :turbo "Turbo")
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "crash" [msg]
  (when (= (:color @brain/track-parameters) (get-in msg [:data :color]))
    (println (prn-str msg))
    (let [first-piece (brain/following-piece 0)
          target-speed (if (:radius first-piece)
                         (brain/target-speed-for-piece
                           true
                           (brain/find-lane-coefficient (brain/current-lane))
                           [first-piece (brain/following-piece 1)])
                         999.0)]
      (cond
        (< (:speed @brain/current-location) (+ 0.3 target-speed))
        (brain/adjust-parameters :down :friction)
        (> (:speed @brain/current-location) (+ 0.3 target-speed))
        (brain/adjust-parameters :down :breaking)
        :default :noop)))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg "gameEnd" [msg]
  (println (prn-str @brain/track-parameters))
  (println (prn-str msg))
  {:msgType "ping" :data "ping"})

(defmethod handle-msg :default [msg]
  (println (prn-str msg))
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    #_(send-message channel
                 {:msgType "joinRace"
                  :data {:botId {:name botname :key botkey}
                         :trackName "germany"
                         :carCount 1}})
    (game-loop channel)))
