(ns hwo2014bot.brain
  (:import [java.lang Math]))

(def game-data (atom {}))
(def current-location (atom {:piece 0 :location 0.0 :speed 0.0 :angle 0.0}))
;; maximum centripetal force (friction coefficient) seems to be 0.47
;; lifting the gas completely seems to break at 0.02 speedunit/length unit
(def track-parameters (atom {:breaking {:breaking-speed 0.02
                                        :adjustment-amount 0.1 :last-direction :down}
                             :friction {:friction-coefficient 0.30
                                        :adjustment-amount 0.40 :last-direction :none}
                             :max-angle-during-lap 0.0}))

(defn- get-my-data [msg] (some #(when (= (:color @track-parameters) (get-in % [:id :color])) %) (:data msg)))
(defn- abs [n] (if (neg? n) (* -1.0 n) n))
(defn- lap [my-data] (get-in my-data [:piecePosition :lap]))

(def statistics (atom {}))

;; Adjust parameters

(defn adjust-parameters [direction type]
  (let [amount (if (= direction (get-in @track-parameters [type :last-direction]))
                 (get-in @track-parameters [type :adjustment-amount])
                 (do (swap! track-parameters assoc-in [type :last-direction] direction)
                   (get-in
                     (swap! track-parameters update-in [type :adjustment-amount] * 0.5)
                     [type :adjustment-amount])))
        signed-amount (if (= direction :up) amount (* -1.0 amount))]
    (println (str "Adjusting parameters " direction " " type " " (+ 1.0 signed-amount)))
    (println (prn-str @track-parameters)) 
    (swap! track-parameters update-in (if (= type :breaking)
                                        [:breaking :breaking-speed]
                                        [:friction :friction-coefficient]) * (+ 1.0 signed-amount))))

(defn- try-adjust [angle]
  (if (> angle (:max-angle-during-lap @track-parameters))
    (adjust-parameters :up :friction)
    (swap! track-parameters update-in [:friction :adjustment-amount] * 0.9)))

(defn- current-location-watcher [k r old-val new-val]
  (cond
    (= (:my-data old-val) (:my-data new-val)) :noop
    (not (:my-data old-val)) :noop
    (not= (lap (:my-data new-val)) (lap (:my-data old-val)))
    (do
      (try-adjust 50.0)
      (swap! track-parameters assoc :max-angle-during-lap 35.0))
    (and (not= (:piece old-val) (:piece new-val))
         (or (not (:adjustment-piece @track-parameters))
             (and (not= (:adjustment-piece @track-parameters) (:piece old-val))
                  (not= (:adjustment-piece @track-parameters) (:piece new-val))))
         (== (quot (:piece-count @track-parameters) 2) (inc (:piece new-val))))
    (do
      (try-adjust 20.0)
      (swap! track-parameters assoc :adjustment-piece (:piece new-val)))
    (== (lap (:my-data new-val)) (lap (:my-data old-val)))
    (swap! track-parameters update-in [:max-angle-during-lap] max (:angle new-val))))

(add-watch current-location :location-watch-key current-location-watcher)

;; Create location

(defn- create-location [old-location msg]
  (let [my-data (get-my-data msg)
        ang (abs (:angle my-data))
        loc (get-in my-data [:piecePosition :inPieceDistance])
        piece (get-in my-data [:piecePosition :pieceIndex])
        speed (if (== (:piece old-location) piece)
                (- loc (:location old-location))
                (:speed old-location))
        previous-data (:my-data old-location)]
    {:piece piece
     :location loc
     :angle ang
     :speed speed
     :acc (- speed (:speed old-location))
     :my-data my-data}))

;; Utilities

(defn current-lane [] (get-in (:my-data @current-location) [:piecePosition :lane :endLaneIndex]))
(defn- current-lap [] (lap (:my-data @current-location)))
(defn- number-of-laps-in-race [] (get-in @game-data [:data :race :raceSession :laps] 9999))

(defn following-piece [index]
  (let [idx (+ (:piece @current-location) index)
        pieces (get-in @game-data [:data :race :track :pieces])
        piece-count (:piece-count @track-parameters)
        mod-idx (if (and (not (== (current-lap) (dec (number-of-laps-in-race))))
                         (< (dec piece-count) idx))
                  (- idx piece-count)
                  idx)]
    (get (get-in @game-data [:data :race :track :pieces])
         mod-idx)))

(defn- breaking-distance-to-speed
  [breaking-dec-speed current-speed target-speed]
  (let [speed-diff (- current-speed target-speed)]
    (if (pos? speed-diff)
      (+ (/ speed-diff breaking-dec-speed) current-speed)
      -1.0)))

(defn- relative-coefficient [signed-angle lane-coefficient]
  (if (pos? signed-angle) (* -1 lane-coefficient) lane-coefficient))

(defn- length-of-piece [piece lane-coefficient]
  (or (:length piece)
      (abs (/ (* 3.14159265359
                 (+ (:radius piece)
                    (relative-coefficient (:angle piece) lane-coefficient))
                 (:angle piece))
              180.0))))

(defn- distance-to-piece-with-index [msg index lane-coefficient]
  (apply + (for [idx (range (inc index))
                 :let [length (length-of-piece (following-piece idx) 0)]]
             (cond
               (== index 0) (:location @current-location)
               (== idx 0) (- length (:location @current-location))
               (== idx index) 0.0
               :default length))))

(defn- find-optimal-speed [radius percentage]
  (java.lang.Math/sqrt (* radius (* percentage (get-in @track-parameters [:friction :friction-coefficient])))))

(defn find-lane-coefficient [lane]
  (some #(when (== (:index %) lane) (:distanceFromCenter %))
        (get-in @game-data [:data :race :track :lanes])))

(defn target-speed-for-piece [current-piece lane-coefficient [{radius :radius signed-angle :angle}
                                                              {s-radius :radius s-signed-angle :angle}]]
  (let [angle (abs signed-angle)
        double-angle (if s-signed-angle (abs (+ s-signed-angle signed-angle)) 0.0)
        lane-coefficient (relative-coefficient signed-angle lane-coefficient)
        radius (+ lane-coefficient radius)
        optimal-speed (find-optimal-speed radius 1.0)]
    #_(print (prn-str ["Wild" angle double-angle radius optimal-speed]))
    (cond
      (< 40.0 angle 80.0 double-angle) optimal-speed
      (< 20.0 angle 40.0 double-angle) optimal-speed
      (and current-piece (< 40 (:angle @current-location))) optimal-speed
      (< 40.0 angle) (find-optimal-speed radius 0.70)
      :default 9999.0)))

(defn- find-val-for-index [msg pieces index-for-piece-to-break-for]
  (if (> 15.0 (abs (or (:angle (following-piece index-for-piece-to-break-for)) 0.0)))
    :R
    (let [lane-coefficient (find-lane-coefficient (current-lane))
          distance-to-piece
          (distance-to-piece-with-index msg index-for-piece-to-break-for lane-coefficient)
          distance-to-reach-speed
          (breaking-distance-to-speed (get-in @track-parameters [:breaking :breaking-speed])
                                      (:speed @current-location)
                                      (target-speed-for-piece
                                        (== 0 index-for-piece-to-break-for)
                                        lane-coefficient
                                        (nth pieces index-for-piece-to-break-for)))]
      #_(print (prn-str ["Style" index-for-piece-to-break-for distance-to-piece distance-to-reach-speed]))
      (if (< (max 0.0 distance-to-piece)
             distance-to-reach-speed)
        :B
        :R))))

;; Drivers

(defn- get-throttle [msg]
  (if (some #{:B} (map (partial find-val-for-index
                                msg
                                (partition 2 1 (map following-piece [0 1 2 3])))
                       [0 1 2]))
    0.0
    1.0))

;; Switch lanes

(def switch (atom nil))

(defn- switch-lane [msg current-piece-index]
  (let [four-seqs (take 4 (partition-by
                            :switch
                            (drop current-piece-index
                                  (cycle (get-in @game-data [:data :race :track :pieces])))))
        switch-index (+ (* 1000 (current-lap))
                        (count (first four-seqs))
                        current-piece-index)
        rad (apply + (map :angle (filter :angle (apply concat (drop 1 four-seqs)))))]
    (cond
      (> 2.0 (:speed @current-location)) nil
      (get @switch switch-index) nil
      (:switch (following-piece 0)) nil
      (< 10.0 rad) nil #_(do
                          (reset! switch {switch-index :switch-sent})
                          "Right")
      (> -10.0 rad) nil #_(do
                           (reset! switch {switch-index :switch-sent})
                           "Left")
      :default (do
                 (reset! switch {switch-index :switch-sent})
                 (rand-nth ["Left" "Right"])))))

;; Controller

(defn handle-msg [msg]
  (swap! current-location create-location msg)
  (swap! current-location
         assoc
         :throttle (get-throttle msg)
         :switch-lane (switch-lane msg (:piece @current-location)))
  #_(when (and (:turbo @track-parameters)
              (not (seq (filter :radius (map following-piece [0 1 2 3 4])))))
     (swap! track-parameters dissoc :turbo)
     (swap! current-location assoc :turbo true))
  #_(print (prn-str [@current-location @track-parameters]))
  (cond
    (:switch-lane @current-location)
    {:msgType "switchLane"
     :data (:switch-lane @current-location)}
    (:turbo @current-location)
    {:msgType "turbo" :data "Turbo"}
    :default
    {:msgType "throttle"
     :data (:throttle @current-location)}))
